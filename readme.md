# Honcho Lumen package

A package for communicating with Honcho for Lumen.

## Installation

Add the following to composer.json

    "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:teamtoursdirect/honcho-lumen.git"
            }
    ]
        
`composer require ttd/honcho-lumen:dev-master`

Edit bootstrap/app.php and add 

    $app->register(\TTD\HonchoLumen\HonchoLumenServiceProvider::class);

