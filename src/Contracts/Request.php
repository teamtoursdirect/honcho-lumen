<?php

namespace TTD\HonchoLumen\Contracts;

/**
 * Interface Request
 *
 * @package TTD\HonchoLumen\Contracts
 */
interface Request
{
    /**
     * Get a URL and return the json
     *
     * @param string $url
     * @param array  $headers
     * @return \stdClass
     * @throws \TTD\HonchoLumen\Exceptions\RequestException
     */
    public function get(string $url, array $headers = []);

    /**
     * Do a post request and return the json
     *
     * @param string $url
     * @param array  $data
     * @param array  $headers
     * @return \stdClass
     * @throws \TTD\HonchoLumen\Exceptions\RequestException
     * @throws \TTD\HonchoLumen\Exceptions\ValidationException
     */
    public function post(string $url, array $data = [], array $headers = []);
}
