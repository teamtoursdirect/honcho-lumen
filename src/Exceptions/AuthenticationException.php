<?php

namespace TTD\HonchoLumen\Exceptions;

/**
 * Class AuthenticationException
 *
 * @package TTD\HonchoLumen\Exceptions
 */
class AuthenticationException extends RequestException
{

}
