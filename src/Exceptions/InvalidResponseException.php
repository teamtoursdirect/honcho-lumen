<?php

namespace TTD\HonchoLumen\Exceptions;

/**
 * Class InvalidResponseException
 *
 * @package TTD\HonchoLumen\Exceptions
 */
class InvalidResponseException extends RequestException
{

}
