<?php

namespace TTD\HonchoLumen\Exceptions;

/**
 * Class RequestException
 *
 * @package TTD\HonchoLumen\Exceptions
 */
class RequestException extends HonchoException
{

}
