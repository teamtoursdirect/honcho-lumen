<?php

namespace TTD\HonchoLumen\Exceptions;

/**
 * Thrown when a 422 response is returned from the API.
 *
 * @package TTD\HonchoLumen\Exceptions
 */
class ValidationException extends RequestException
{
    /**
     * @var mixed
     */
    public $errors;

    /**
     * ValidationException constructor.
     *
     * @param string $errors
     */
    public function __construct($errors)
    {
        $this->errors = $errors;
    }
}
