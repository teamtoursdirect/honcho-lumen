<?php

namespace TTD\HonchoLumen;

use TTD\HonchoLumen\Contracts\Request;
use TTD\HonchoLumen\Exceptions\InvalidResponseException;

/**
 * Class Honcho
 *
 * @package TTD\HonchoLumen
 */
class Honcho
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var \TTD\HonchoLumen\Contracts\Request
     */
    protected $request;

    /**
     * Honcho constructor.
     *
     * @param \TTD\HonchoLumen\Contracts\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param \TTD\HonchoLumen\Contracts\Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function setKey(string $key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param string $secret
     * @return $this
     */
    public function setSecret(string $secret)
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     * @throws \TTD\HonchoLumen\Exceptions\InvalidResponseException
     */
    public function get()
    {
        $response = $this->request->get($this->url, [
            'api-key' => $this->getKey(),
            'api-secret' => $this->getSecret(),
        ]);

        if (isset($response->data, $response->data->datasets)) {
            return $response->data->datasets;
        }

        throw new InvalidResponseException;
    }

    /**
     * @param array  $data
     * @param string $type
     * @param string $provider
     * @return mixed
     * @throws \TTD\HonchoLumen\Exceptions\InvalidResponseException
     */
    public function create(array $data, string $type, string $provider)
    {
        $postData = [
            'data' => json_encode($data),
            'provider' => $provider,
            'type' => $type
        ];

        $response = $this->request->post($this->url, $postData, [
            'api-key' => $this->getKey(),
            'api-secret' => $this->getSecret(),
        ]);

        if (isset($response->data, $response->data->stored)) {
            return $response->data->stored;
        }

        throw new InvalidResponseException;
    }
}
