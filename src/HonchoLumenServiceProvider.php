<?php

namespace TTD\HonchoLumen;

use Illuminate\Support\ServiceProvider;

/**
 * Class HonchoLumenServiceProvider
 *
 * @package TTD\HonchoLumen
 */
class HonchoLumenServiceProvider extends ServiceProvider
{
    /**
     * Register the config and bind the classes
     */
    public function boot()
    {
        $this->mergeConfigFrom($this->getBasePath('config/honcho.php'), 'honcho');
        $this->bind();
    }

    /**
     * Bind the classes
     */
    protected function bind()
    {
        // bind the request class
        $requestName = $this->treatName($this->getRequestDriverName());
        $this->app->singleton('TTD\HonchoLumen\Contracts\Request', function ($app) use ($requestName) {
            return $app->make('TTD\HonchoLumen\Request\\' . $requestName);
        });

        // bind the Honcho class *last* as a singleton...
        $this->app->singleton('TTD\HonchoLumen\Honcho');

        $this->app->make('TTD\HonchoLumen\Honcho')
            ->setUrl($this->getApiConfig()['url'])
            ->setKey($this->getApiConfig()['key'])
            ->setSecret($this->getApiConfig()['secret']);
    }

    /**
     * Turn a string into StudlyCase
     *
     * @param string $name
     * @return string
     */
    protected function treatName(string $name)
    {
        return studly_case($name);
    }

    /**
     * @return string
     */
    protected function getRequestDriverName(): string
    {
        return config('honcho.request.driver');
    }

    /**
     * @return array
     */
    protected function getApiConfig(): array
    {
        return config('honcho.api');
    }

    /**
     * Get the base path of this package
     *
     * @param string $path
     * @return string
     */
    protected function getBasePath($path = '')
    {
        return rtrim(dirname(__FILE__), '/') . '/' . ltrim($path, '/');
    }
}
