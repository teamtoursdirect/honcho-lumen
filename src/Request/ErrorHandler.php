<?php

namespace TTD\HonchoLumen\Request;

use TTD\HonchoLumen\Exceptions\RequestException;
use TTD\HonchoLumen\Exceptions\ValidationException;
use TTD\HonchoLumen\Exceptions\AuthenticationException;

/**
 * Class ErrorHandler
 *
 * @package TTD\HonchoLumen\Request
 */
abstract class ErrorHandler
{
    /**
     * If a method exists for the code, call that. Otherwise, just call the generic ResponseException
     *
     * @param int  $responseCode
     * @param null $body
     * @throws mixed
     */
    public function handleError(int $responseCode, $body = null)
    {
        // if the method exists, call that method instead
        if (method_exists($this, 'handle' . $responseCode)) {
            $this->{'handle' . $responseCode}($body);
        }

        throw new RequestException("Request failed with response code " . $responseCode);
    }

    /**
     * @throws \TTD\HonchoLumen\Exceptions\AuthenticationException
     */
    public function handle401()
    {
        throw new AuthenticationException;
    }

    /**
     * @param null $body
     * @throws \TTD\HonchoLumen\Exceptions\ValidationException
     */
    public function handle422($body = null)
    {
        throw new ValidationException(json_decode($body)->validation);
    }
}
