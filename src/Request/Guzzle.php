<?php

namespace TTD\HonchoLumen\Request;

use GuzzleHttp\Client;
use TTD\HonchoLumen\Contracts\Request;
use GuzzleHttp\Exception\RequestException;
use TTD\HonchoLumen\Exceptions\InvalidResponseException;

/**
 * Class Guzzle
 *
 * @package TTD\HonchoLumen\Request
 */
class Guzzle extends ErrorHandler implements Request
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * Guzzle constructor.
     *
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param \GuzzleHttp\Client $client
     * @return $this
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get a URL. If the URL is
     *
     * @param string $url
     * @param array  $headers
     * @return \stdClass
     * @throws \TTD\HonchoLumen\Exceptions\ValidationException
     * @throws \TTD\HonchoLumen\Exceptions\RequestException
     */
    public function get(string $url, array $headers = [])
    {
        return $this->request('get', $url, [
            'headers' => $headers
        ]);
    }

    /**
     * Do a post request and return the json
     *
     * @param string $url
     * @param array  $data
     * @param array  $headers
     * @return \stdClass
     * @throws \TTD\HonchoLumen\Exceptions\RequestException
     * @throws \TTD\HonchoLumen\Exceptions\ValidationException
     */
    public function post(string $url, array $data = [], array $headers = [])
    {
        return $this->request('post', $url, [
            'headers' => $headers,
            'form_params' => $data
        ]);
    }

    /**
     * Do a request via Guzzle
     *
     * @param string $method
     * @param string $url
     * @param array  $data
     * @return mixed
     * @throws \TTD\HonchoLumen\Exceptions\InvalidResponseException
     */
    protected function request(string $method, string $url, array $data = [])
    {
        try {
            $request = $this->client->$method($url, $data);
        } catch (RequestException $e) {
            $this->handleError($e->getCode(), $e->getResponse()->getBody());
        }

        $body = json_decode($request->getBody());

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new InvalidResponseException;
        }

        return $body;
    }
}
