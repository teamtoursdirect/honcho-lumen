<?php

return [
    /*
     * The Honcho API config
     */
    'api' => [
        /*
         * The API url
         */
        'url' => env('HONCHO_API_URL', 'https://honcho.sertxcrm.com'),

        /*
         * The API key.
         * This needs to be unique for each consumer that pulls from Honcho.
         */
        'key' => env('HONCHO_API_KEY', null),

        /*
         * The API secret.
         * Keep it secret, keep it safe.
         */
        'secret' => env('HONCHO_API_SECRET', null),
    ],

    /*
     * Request config
     */
    'request' => [
        /*
         * What driver to use.
         * Currently supports: guzzle
         */
        'driver' => 'guzzle'
    ],

    'events' => [
        /*
         * An array of events to be fired *for each* dataset retrieved from Honcho.
         */
        'get' => [],

        /*
         * An array of events to be fired when a successful post to Honcho has happened.
         */
        'post' => [],
    ]
];
