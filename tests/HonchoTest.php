<?php

class HonchoTest extends \Tests\TestCase
{
    /**
     * @var \TTD\HonchoLumen\Honcho
     */
    protected $honcho;

    /**
     * @var \TTD\HonchoLumen\Contracts\Request|\Mockery\Mock
     */
    protected $request;

    public function setUp()
    {
        parent::setUp();
        $this->request = Mockery::mock('TTD\HonchoLumen\Contracts\Request');
        $this->honcho = app('TTD\HonchoLumen\Honcho');
        $this->honcho->setRequest($this->request);

        $this->honcho->setUrl('aaa');
        $this->honcho->setKey('12');
        $this->honcho->setSecret('34');
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\InvalidResponseException
     */
    public function test_get_bad_response()
    {
        $this->request->shouldReceive('get')->once()->with('aaa', [
            'api-key' => '12',
            'api-secret' => '34',
        ])->andReturn(null);

        $this->honcho->get();
    }

    public function test_get()
    {
        $this->request->shouldReceive('get')->once()->with('aaa', [
            'api-key' => '12',
            'api-secret' => '34',
        ])->andReturn(json_decode(json_encode([
            'data' => ['datasets' => 'xxx']
        ])));

        $this->assertEquals('xxx', $this->honcho->get());
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\InvalidResponseException
     */
    public function test_post_bad_response()
    {
        $this->request->shouldReceive('post')->once()->with('aaa', [
            'data' => json_encode(['a' => 'b']),
            'provider' => 'x',
            'type' => 'y'
        ], [
            'api-key' => '12',
            'api-secret' => '34',
        ])->andReturn(null);

        $this->honcho->create(['a' => 'b'], 'y', 'x');
    }

    public function test_post()
    {
        $this->request->shouldReceive('post')->once()->with('aaa', [
            'data' => json_encode(['a' => 'b']),
            'provider' => 'x',
            'type' => 'y'
        ], [
            'api-key' => '12',
            'api-secret' => '34',
        ])->andReturn(json_decode(json_encode([
            'data' => ['stored' => 'xxx']
        ])));

        $this->assertEquals('xxx', $this->honcho->create(['a' => 'b'], 'y', 'x'));
    }
}
