<?php

class ErrorHandlerTest extends \Tests\TestCase
{
    /**
     * @var \ErrorHandlerStub
     */
    protected $handler;

    public function setUp()
    {
        parent::setUp();
        $this->handler = new ErrorHandlerStub;
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\AuthenticationException
     */
    public function test_handle_401()
    {
        $this->handler->handle401();
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\AuthenticationException
     */
    public function test_handle_401_through_handle_error()
    {
        $this->handler->handleError(401);
    }

    public function test_handle_422()
    {
        try {
            $this->handler->handle422(json_encode(['validation' => ['a' => 'b']]));
        } catch (\TTD\HonchoLumen\Exceptions\ValidationException $e) {
            $this->assertEquals((object)['a' => 'b'], $e->errors);
        }
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\RequestException
     */
    public function test_handle_error()
    {
        $this->handler->handleError(0);
    }
}

class ErrorHandlerStub extends \TTD\HonchoLumen\Request\ErrorHandler
{

}
