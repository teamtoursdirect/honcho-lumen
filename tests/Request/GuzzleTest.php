<?php

class GuzzleTest extends \Tests\TestCase
{
    /**
     * @var \TTD\HonchoLumen\Request\Guzzle
     */
    protected $guzzle;

    public function setUp()
    {
        parent::setUp();
        $this->guzzle = app('TTD\HonchoLumen\Request\Guzzle');
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\RequestException
     */
    public function test_get_fail_call()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Exception\RequestException(
                'Some error',
                new \GuzzleHttp\Psr7\Request('GET', 'aaa'),
                new \GuzzleHttp\Psr7\Response('aaa')
            )
        ]);
        $client = new \GuzzleHttp\Client(['handler' => \GuzzleHttp\HandlerStack::create($mock)]);
        $this->guzzle->setClient($client);

        $this->guzzle->get('aaa', ['a' => 'b']);
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\InvalidResponseException
     */
    public function test_get_bad_json()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], 'aaaaa')
        ]);
        $client = new \GuzzleHttp\Client(['handler' => \GuzzleHttp\HandlerStack::create($mock)]);
        $this->guzzle->setClient($client);

        $this->guzzle->get('aaa', ['a' => 'b']);
    }

    public function test_get()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], json_encode(['c' => 'd']))
        ]);
        $client = new \GuzzleHttp\Client(['handler' => \GuzzleHttp\HandlerStack::create($mock)]);
        $this->guzzle->setClient($client);

        $this->assertEquals((object)['c' => 'd'], $this->guzzle->get('aaa', ['a' => 'b']));
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\RequestException
     */
    public function test_post_fail_call()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Exception\RequestException(
                'Some error',
                new \GuzzleHttp\Psr7\Request('GET', 'aaa'),
                new \GuzzleHttp\Psr7\Response('aaa')
            )
        ]);
        $client = new \GuzzleHttp\Client(['handler' => \GuzzleHttp\HandlerStack::create($mock)]);
        $this->guzzle->setClient($client);

        $this->guzzle->post('aaa', ['a' => 'b'], ['c' => 'd']);
    }

    /**
     * @expectedException \TTD\HonchoLumen\Exceptions\InvalidResponseException
     */
    public function test_post_bad_json()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], 'aaaaa')
        ]);
        $client = new \GuzzleHttp\Client(['handler' => \GuzzleHttp\HandlerStack::create($mock)]);
        $this->guzzle->setClient($client);

        $this->guzzle->post('aaa', ['a' => 'b'], ['c' => 'd']);
    }

    public function test_post()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], json_encode(['c' => 'd']))
        ]);
        $client = new \GuzzleHttp\Client(['handler' => \GuzzleHttp\HandlerStack::create($mock)]);
        $this->guzzle->setClient($client);

        $this->assertEquals((object)['c' => 'd'], $this->guzzle->post('aaa', ['a' => 'b'], ['c' => 'd']));
    }
}
